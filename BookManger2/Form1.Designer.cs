﻿namespace BookManger2
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bookDataGrid = new System.Windows.Forms.DataGridView();
            this.bookDataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.bookName = new System.Windows.Forms.TextBox();
            this.author = new System.Windows.Forms.TextBox();
            this.price = new System.Windows.Forms.MaskedTextBox();
            this.addButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.searchBox = new System.Windows.Forms.TextBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.review = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.bookDataTableBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.bookDataTableBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.bookDataTableBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.bookDataTableBindingSource4 = new System.Windows.Forms.BindingSource(this.components);
            this.bookDataTableBindingSource5 = new System.Windows.Forms.BindingSource(this.components);
            this.reviewBox = new System.Windows.Forms.MaskedTextBox();
            this.bookDataTableBindingSource6 = new System.Windows.Forms.BindingSource(this.components);
            this.genretag = new System.Windows.Forms.Label();
            this.genreBox = new System.Windows.Forms.TextBox();
            this.bookDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bookDataSet = new BookManger2.BookDataSet();
            this.bookDataSet1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bookDataSet1 = new BookManger2.BookDataSet();
            this.bookDataSet2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bookDataSet2 = new BookManger2.BookDataSet();
            this.bookDataSet3BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bookDataSet3 = new BookManger2.BookDataSet();
            this.bookDataSet3BindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.bookDataSet3BindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.bookDataSet3BindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.書名DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.著者DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.値段DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.登録日DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.評価DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.カテゴリDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataTableBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataTableBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataTableBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataTableBindingSource4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataTableBindingSource5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataTableBindingSource6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet2BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet3BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet3BindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet3BindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet3BindingSource3)).BeginInit();
            this.SuspendLayout();
            // 
            // bookDataGrid
            // 
            this.bookDataGrid.AutoGenerateColumns = false;
            this.bookDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bookDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.書名DataGridViewTextBoxColumn,
            this.著者DataGridViewTextBoxColumn,
            this.値段DataGridViewTextBoxColumn,
            this.登録日DataGridViewTextBoxColumn,
            this.評価DataGridViewTextBoxColumn,
            this.カテゴリDataGridViewTextBoxColumn});
            this.bookDataGrid.DataSource = this.bookDataTableBindingSource;
            this.bookDataGrid.Location = new System.Drawing.Point(211, 46);
            this.bookDataGrid.Name = "bookDataGrid";
            this.bookDataGrid.RowTemplate.Height = 21;
            this.bookDataGrid.Size = new System.Drawing.Size(771, 425);
            this.bookDataGrid.TabIndex = 0;
            // 
            // bookDataTableBindingSource
            // 
            this.bookDataTableBindingSource.DataMember = "bookDataTable";
            this.bookDataTableBindingSource.DataSource = this.bookDataSetBindingSource;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "書名";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "著名";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "値段";
            // 
            // bookName
            // 
            this.bookName.Location = new System.Drawing.Point(77, 43);
            this.bookName.Name = "bookName";
            this.bookName.Size = new System.Drawing.Size(100, 19);
            this.bookName.TabIndex = 8;
            // 
            // author
            // 
            this.author.Location = new System.Drawing.Point(77, 85);
            this.author.Name = "author";
            this.author.Size = new System.Drawing.Size(100, 19);
            this.author.TabIndex = 9;
            // 
            // price
            // 
            this.price.Location = new System.Drawing.Point(77, 125);
            this.price.Mask = "00000";
            this.price.Name = "price";
            this.price.Size = new System.Drawing.Size(100, 19);
            this.price.TabIndex = 13;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(37, 243);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(139, 73);
            this.addButton.TabIndex = 15;
            this.addButton.Text = "追加";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.AddButtonClicked);
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(37, 343);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(139, 73);
            this.removeButton.TabIndex = 16;
            this.removeButton.Text = "削除";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.RemoveButtonClicked);
            // 
            // searchBox
            // 
            this.searchBox.Location = new System.Drawing.Point(696, 14);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(205, 19);
            this.searchBox.TabIndex = 18;
            this.searchBox.Text = "著者名を記入してください";
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(907, 12);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(75, 23);
            this.searchButton.TabIndex = 20;
            this.searchButton.Text = "検索";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // review
            // 
            this.review.AutoSize = true;
            this.review.Location = new System.Drawing.Point(35, 178);
            this.review.Name = "review";
            this.review.Size = new System.Drawing.Size(29, 12);
            this.review.TabIndex = 21;
            this.review.Text = "評価";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(121, 181);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 12);
            this.label5.TabIndex = 23;
            this.label5.Text = "(1～5)";
            // 
            // bookDataTableBindingSource1
            // 
            this.bookDataTableBindingSource1.DataMember = "bookDataTable";
            this.bookDataTableBindingSource1.DataSource = this.bookDataSetBindingSource;
            // 
            // bookDataTableBindingSource2
            // 
            this.bookDataTableBindingSource2.DataMember = "bookDataTable";
            this.bookDataTableBindingSource2.DataSource = this.bookDataSet1BindingSource;
            // 
            // bookDataTableBindingSource3
            // 
            this.bookDataTableBindingSource3.DataMember = "bookDataTable";
            this.bookDataTableBindingSource3.DataSource = this.bookDataSet2BindingSource;
            // 
            // bookDataTableBindingSource4
            // 
            this.bookDataTableBindingSource4.DataMember = "bookDataTable";
            this.bookDataTableBindingSource4.DataSource = this.bookDataSet3BindingSource;
            // 
            // bookDataTableBindingSource5
            // 
            this.bookDataTableBindingSource5.DataMember = "bookDataTable";
            this.bookDataTableBindingSource5.DataSource = this.bookDataSetBindingSource;
            // 
            // reviewBox
            // 
            this.reviewBox.Location = new System.Drawing.Point(77, 174);
            this.reviewBox.Mask = "0";
            this.reviewBox.Name = "reviewBox";
            this.reviewBox.Size = new System.Drawing.Size(34, 19);
            this.reviewBox.TabIndex = 24;
            // 
            // bookDataTableBindingSource6
            // 
            this.bookDataTableBindingSource6.DataMember = "bookDataTable";
            this.bookDataTableBindingSource6.DataSource = this.bookDataSetBindingSource;
            // 
            // genretag
            // 
            this.genretag.AutoSize = true;
            this.genretag.Location = new System.Drawing.Point(37, 211);
            this.genretag.Name = "genretag";
            this.genretag.Size = new System.Drawing.Size(39, 12);
            this.genretag.TabIndex = 25;
            this.genretag.Text = "カテゴリ";
            // 
            // genreBox
            // 
            this.genreBox.Location = new System.Drawing.Point(77, 211);
            this.genreBox.Name = "genreBox";
            this.genreBox.Size = new System.Drawing.Size(100, 19);
            this.genreBox.TabIndex = 26;
            // 
            // bookDataSetBindingSource
            // 
            this.bookDataSetBindingSource.DataSource = this.bookDataSet;
            this.bookDataSetBindingSource.Position = 0;
            // 
            // bookDataSet
            // 
            this.bookDataSet.DataSetName = "BookDataSet";
            this.bookDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bookDataSet1BindingSource
            // 
            this.bookDataSet1BindingSource.DataSource = this.bookDataSet1;
            this.bookDataSet1BindingSource.Position = 0;
            // 
            // bookDataSet1
            // 
            this.bookDataSet1.DataSetName = "BookDataSet";
            this.bookDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bookDataSet2BindingSource
            // 
            this.bookDataSet2BindingSource.DataSource = this.bookDataSet2;
            this.bookDataSet2BindingSource.Position = 0;
            // 
            // bookDataSet2
            // 
            this.bookDataSet2.DataSetName = "BookDataSet";
            this.bookDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bookDataSet3BindingSource
            // 
            this.bookDataSet3BindingSource.DataSource = this.bookDataSet3;
            this.bookDataSet3BindingSource.Position = 0;
            // 
            // bookDataSet3
            // 
            this.bookDataSet3.DataSetName = "BookDataSet";
            this.bookDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bookDataSet3BindingSource1
            // 
            this.bookDataSet3BindingSource1.DataSource = this.bookDataSet3;
            this.bookDataSet3BindingSource1.Position = 0;
            // 
            // bookDataSet3BindingSource2
            // 
            this.bookDataSet3BindingSource2.DataSource = this.bookDataSet3;
            this.bookDataSet3BindingSource2.Position = 0;
            // 
            // bookDataSet3BindingSource3
            // 
            this.bookDataSet3BindingSource3.DataSource = this.bookDataSet3;
            this.bookDataSet3BindingSource3.Position = 0;
            // 
            // 書名DataGridViewTextBoxColumn
            // 
            this.書名DataGridViewTextBoxColumn.DataPropertyName = "書名";
            this.書名DataGridViewTextBoxColumn.HeaderText = "書名";
            this.書名DataGridViewTextBoxColumn.Name = "書名DataGridViewTextBoxColumn";
            // 
            // 著者DataGridViewTextBoxColumn
            // 
            this.著者DataGridViewTextBoxColumn.DataPropertyName = "著者";
            this.著者DataGridViewTextBoxColumn.HeaderText = "著者";
            this.著者DataGridViewTextBoxColumn.Name = "著者DataGridViewTextBoxColumn";
            // 
            // 値段DataGridViewTextBoxColumn
            // 
            this.値段DataGridViewTextBoxColumn.DataPropertyName = "値段";
            this.値段DataGridViewTextBoxColumn.HeaderText = "値段";
            this.値段DataGridViewTextBoxColumn.Name = "値段DataGridViewTextBoxColumn";
            // 
            // 登録日DataGridViewTextBoxColumn
            // 
            this.登録日DataGridViewTextBoxColumn.DataPropertyName = "登録日";
            this.登録日DataGridViewTextBoxColumn.HeaderText = "登録日";
            this.登録日DataGridViewTextBoxColumn.Name = "登録日DataGridViewTextBoxColumn";
            // 
            // 評価DataGridViewTextBoxColumn
            // 
            this.評価DataGridViewTextBoxColumn.DataPropertyName = "評価";
            this.評価DataGridViewTextBoxColumn.HeaderText = "評価";
            this.評価DataGridViewTextBoxColumn.Name = "評価DataGridViewTextBoxColumn";
            // 
            // カテゴリDataGridViewTextBoxColumn
            // 
            this.カテゴリDataGridViewTextBoxColumn.DataPropertyName = "カテゴリ";
            this.カテゴリDataGridViewTextBoxColumn.HeaderText = "カテゴリ";
            this.カテゴリDataGridViewTextBoxColumn.Name = "カテゴリDataGridViewTextBoxColumn";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1033, 498);
            this.Controls.Add(this.genreBox);
            this.Controls.Add(this.genretag);
            this.Controls.Add(this.reviewBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.review);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.searchBox);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.price);
            this.Controls.Add(this.author);
            this.Controls.Add(this.bookName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bookDataGrid);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.bookDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataTableBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataTableBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataTableBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataTableBindingSource4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataTableBindingSource5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataTableBindingSource6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet2BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet3BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet3BindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet3BindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataSet3BindingSource3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView bookDataGrid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox bookName;
        private System.Windows.Forms.TextBox author;
        private System.Windows.Forms.MaskedTextBox price;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.TextBox searchBox;
        private System.Windows.Forms.BindingSource bookDataSetBindingSource;
        private BookDataSet bookDataSet;
        private System.Windows.Forms.BindingSource bookDataSet1BindingSource;
        private BookDataSet bookDataSet1;
        private System.Windows.Forms.DataGridViewTextBoxColumn 合計金額DataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource bookDataTableBindingSource;
        private BookDataSet bookDataSet2;
        private System.Windows.Forms.BindingSource bookDataSet2BindingSource;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Label review;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.BindingSource bookDataTableBindingSource5;
        private BookDataSet bookDataSet3;
        private System.Windows.Forms.BindingSource bookDataSet3BindingSource;
        private System.Windows.Forms.BindingSource bookDataTableBindingSource1;
        private System.Windows.Forms.BindingSource bookDataTableBindingSource2;
        private System.Windows.Forms.BindingSource bookDataTableBindingSource3;
        private System.Windows.Forms.BindingSource bookDataTableBindingSource4;
        private System.Windows.Forms.BindingSource bookDataSet3BindingSource1;
        private System.Windows.Forms.BindingSource bookDataSet3BindingSource2;
        private System.Windows.Forms.MaskedTextBox reviewBox;
        private System.Windows.Forms.BindingSource bookDataSet3BindingSource3;
        private System.Windows.Forms.BindingSource bookDataTableBindingSource6;
        private System.Windows.Forms.Label genretag;
        private System.Windows.Forms.TextBox genreBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn 書名DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 著者DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 値段DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 登録日DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 評価DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn カテゴリDataGridViewTextBoxColumn;
    }
}

