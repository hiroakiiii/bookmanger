﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManger2
{
    public partial class Form1 : Form
    {
        public Form1()
            
        {
            InitializeComponent();
        }

        private void AddButtonClicked(object sender, EventArgs e)
        {
            bookDataSet.bookDataTable.AddbookDataTableRow(
                this.bookName.Text,
                this.author.Text,
                int.Parse(this.price.Text),
                DateTime.Now.ToString("yyyy年MM月dd日"),
                int.Parse(this.reviewBox.Text),
                this.genreBox.Text

                );
        }

        private void RemoveButtonClicked(object sender, EventArgs e)
        {
            //選択した行のデータを削除する
            int row = this.bookDataGrid.CurrentRow.Index;
            this.bookDataGrid.Rows.RemoveAt(row);
        }

        //public void searchResult(object sender, EventArgs e)
        
        //{

        //}

        public void searchButton_Click(object sender, EventArgs e)
        {
            Console.WriteLine("著者 Like '% " + searchBox.Text + " %'");
            DataRow[] rows = bookDataSet.Tables["bookDataTable"].Select("著者 Like '%" + searchBox.Text + "%'");
            
           for (int i = 0; i<rows.Length;i++)
            {
                Console.WriteLine(rows[i]);
            }

            BindingSource source = new BindingSource();
            source.DataSource = rows;
            bookDataGrid.DataSource = source;

        }
        
          
    }
}
